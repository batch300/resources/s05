<?php
session_start();
$user = $_SESSION['username'];
  
  if(isset($_POST['btnLogout'])) {
       
       session_destroy();
       header('Location: ./index.php');
  }
 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
       
       <form method="POST" action="server.php">
       	 <h2>Hi <?php echo $user; ?></h2>
       <button type="submit" name="btnLogout">Logout</button>
       </form>
      
</body>
</html>