<!-- DEBUG THE CODES SO THAT YOU MAY VIEW THE BOTH THE RETRIEVED TASKS IN GET AND POST WITH ITS CORRESPONDING OPTION VALUE/INDEX, YOU MAY REFER TO THE EXPECTED OUTPUT IN THE HANGOUTS -->

<!-- Paste this to an index.php file  -->

<?php
	$tasks = ["Get Git", "Bake HTML", "Eat CSS","Learn PHP"];

	//php is constantly checking if there is a data passed in the url

	//$_POST and $_GET are "super global variables" in PHP
		//these allow us to retrieve information sent bt thw client

	//Both $_GET and $_POST (super global variables) allows data to persist between pages or within a single session

	//The main difference between $_GET and $_POST is that $_GET appends the URL with "?param=value"
		//If you are to send sensitive data to the server, $_GET is not recommended

	//isset() function checks whether a variable is a set
		//this is just used to return s boolean value

	if(isset($_GET["index"])){
		$indexGet = $_GET["index"];

		//we get the value of the $_GET["index"]
		//index is the name of our input which the value is whatever the user selects
		//$tasks[0]
		echo "The retrieved task from GET is $tasks[$indexGet]. <br>";
	}

	if(isset($_POST["index"])){
		$indexPost = $_POST["index"];
		echo "The retrieved task from POST is $tasks[$indexPost]. <br>";
	}

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>($_GET and $_POST)</title>
	</head>
	<body>
	    
		<h1>Task index from GET</h1>
		<form method="GET">
			<select name="index" required>
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>

			<button type="submit">GET</button>
		</form>

		<h1>Task index from POST</h1>
		<form method="POST">
			<select name="index" required>
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>

			<button type="submit">POST</button>
		</form>
	</body>
</html>
