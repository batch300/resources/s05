<p>Hi! Welcome to server.php</p><br>

<?php

//to start the session
session_start();
// echo $_POST['description'];

//We are going to apply OOP and create a class

class TaskList {

	public function add($description){

		$newTask = (object)[
			'description' => $description,
			'isFinished' => false
		];


		if($_SESSION['tasks'] === null){
			$_SESSION['tasks'] = array();
		}

		array_push($_SESSION['tasks'],$newTask);
	}

	public function update($id,$description,$isFinished) {
		$_SESSION['tasks'][$id]->description = $description;
		$_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
	}

	public function remove($id) {
		array_splice($_SESSION['tasks'],$id,1);
	}

	public function clear(){
		session_destroy();
	}

}

//create an instance to use the methods

$taskList = new TaskList();


//handle post variable actions
	//input with the value add
if($_POST['action'] === 'add'){

	$taskList->add($_POST['description']);

} else if($_POST['action'] === 'update'){

	$taskList->update($_POST['id'],$_POST['description'],$_POST['isFinished']);

} else if($_POST['action'] === 'remove'){

	$taskList->remove($_POST['id']);

} else if($_POST['action'] === 'clear'){

	$taskList->clear();
}

//Header
//We use this to redirect to another page
header('Location: ./index.php');